import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { SmartpayTranslateLoader } from '../providers/smartpay-translate-loader';
import { Http, HttpModule } from '@angular/http';
import { Auth } from '../providers/auth';
import { Profile } from '../providers/profile';

import { Payment } from '../providers/payment';


import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

export const firebaseConfig = {
  apiKey: "AIzaSyB-PWpatO7tgBpwZd7JVF9B4nhRs8zKrXI",
  authDomain: "smartpay-64b0d.firebaseapp.com",
  databaseURL: "https://smartpay-64b0d.firebaseio.com",
  storageBucket: "smartpay-64b0d.appspot.com",
  messagingSenderId: "909515671580"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (SmartpayTranslateLoader),
        deps: [Http]
      }
    }) 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
    /*
    HomePage,
    ListPage,
    StartPage,
    LoginPage,
    SignupPage,
    SetupPage,
    ProfilePage,
    EditProfile,
    Paybag,
	  Transactions,
    Pay,
    PaymentDetail,
    Receive,
    Help*/
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Auth,
    Profile,    
    Payment,
    InAppBrowser
  ]
})
export class AppModule {}
