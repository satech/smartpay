import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, ActionSheetController } from 'ionic-angular';
//import { Storage } from 'ionic-storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

/*
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StartPage } from '../pages/start/start';
import { SetupPage } from '../pages/setup/setup';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { Paybag } from '../pages/paybag/paybag';*/
import { Auth } from '../providers/auth';

@Component({
  templateUrl: 'app.html',
  //providers: [Auth]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any; // = HomePage;

  pages: Array<{title: string, component: any}>;
  loader: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, 
  public auth: Auth, translate: TranslateService, public loadr: LoadingController, public asc: ActionSheetController) {
    auth.getSeen().then( data => {
      this.auth.isLoggedIn()
        .then((val) => {
          if(val) {
            this.rootPage = 'HomePage';
          } else {
            if(data == 1) {
              this.rootPage = 'LoginPage';
            } else {
              this.rootPage = 'StartPage';    
            }
          }
        });   
      
    });

    translate.setDefaultLang('en');
    translate.use('en');

    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Signing out..."
    });
    
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
      { title: 'Settings', component: 'Options' },
      { title: 'Start', component: 'StartPage' },
      { title: 'Setup', component: 'SetupPage' },
      { title: 'Profile', component: 'ProfilePage' },
      { title: 'Paybag', component: 'Paybag' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    if(this.loader)
      this.loader.present();
    this.auth.logout()
    .then(() => {
      if(this.loader)
        this.loader.dismiss();
      this.nav.setRoot('LoginPage');
    })    
  }

  showMore() {
    let actionSheet = this.asc.create({
      title: 'Shortcuts',
      buttons: [
        { 
          text: 'Settings',
          //role: 'destructive',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.nav.push('Options');
            });
            return false;        
          }
        },{
          text: 'Help',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.nav.push('Help');
            });
            return false;   
          }
        },{
          text: 'Logout',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.logout();
            }); 
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            actionSheet.dismiss().then( () => {  });
            return false;         
          }
        }
      ]
    });
    actionSheet.present();
  }

  openMore() {
    let actionSheet = this.asc.create({
      title: 'Shortcuts',
      buttons: [
        { 
          text: 'Settings',
          //role: 'destructive',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.nav.push('Options');
            });
            return false;   
          }
        },{
          text: 'Help',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.nav.push('Help');
            });
            return false;     
          }
        },{
          text: 'Logout',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.logout();
            });
            return false;  
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            actionSheet.dismiss().then( () => {  });
            return false;          
          }
        }
      ]
    });
    actionSheet.present();
  }
}
