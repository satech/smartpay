import { Component } from '@angular/core';
import { App, IonicPage, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Auth } from '../../providers/auth';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';


/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfile {

  uid: any = null;
  surname: any;
  phoneNumber: any;
  gender: any;
  accountName: any;
  accountNumber: any;
  bankName: any;
  address: any;
  pin: any;
  profile: any;
  formData: any;
  profileForm: any;
  navCtrl: any;
  p: any;

  constructor(public app: App, public navParams: NavParams, public db: AngularFireDatabase, public auth: Auth, public profle: Profile, public fservice: FirebaseService) {
    this.p = {a: 0, b: 1, c: 2};
    let userid = navParams.get('uid');
    this.profile = FirebaseObjectObservable.create(); //this.db.object('/users' + userid + '/profile');    
    this.formData = {};
    this.navCtrl = this.app.getActiveNav();
    let _self = this;
    
    let timeOut = setTimeout(function(e) {
      if(_self.profile && _self.profile.balance) {
        
      }
    }, 5000);
    
    this.fservice.getCurrentUser()
      .take(1)
      .subscribe( (user) => {
        this.uid = user.uid;
        this.fservice.getSavedProfile()
          .take(1)
          .subscribe( (sprofile) => {
            console.log(sprofile);
            console.log(timeOut)
            clearTimeout(timeOut);
            if(sprofile && sprofile.phoneNumber) {
              this.profile = sprofile;
              this.formData = sprofile;           
            }

            this.fservice.getUserProfile(this.uid)
              .take(1)
              .subscribe( (oprofile) => {
                console.log(oprofile);
                clearTimeout(timeOut);            
                if(oprofile && oprofile.phoneNumber) {
                  console.log("1");
                  this.profile = oprofile;
                  this.formData = oprofile;
                } else {
                  
                }
              })
          })
      });

    /*let a = this.auth.getUser()
      .then( (user) => {
		this.uid = user.uid    
    this.profle.getSavedUserProfile()
      .then( (oprofile) => {
        clearTimeout(timeOut);
        if(oprofile) {
          this.formData.firstName = oprofile.firstName;
          this.formData.surname = oprofile.surname;
          this.formData.address = oprofile.address;
          this.profile = oprofile;
        }
        console.log(this.formData);
        
        /*this.fservice.getUserProfile(this.uid)
          .take(1)
          .subscribe( (sprofile) => {
          clearTimeout(timeOut);
          if(sprofile) {
            this.formData.firstName = sprofile.firstName;
            this.formData.surname = sprofile.surname;
            this.formData.address = sprofile.address;
            this.profile = sprofile;
          }
          console.log(this.formData);
          }) */
      

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }

  update() {
    console.log(this.formData);
    this.formData = {};
    if(!this.formData.firstName || !this.formData.surname || !this.formData.address) {
      alert("Please fill all required fields");
      return;
    }

  	/*if(!this.profile.firstName || !this.profile.surname || !this.profile.phoneNumber || !this.profile.gender || !this.profile.address) {
  		alert("Please fill complete all required fields to continue");
  		return;
  	}*/
  	
    let up = this.db.object('/users/' + this.uid + '/profile');
    up.update(this.profile).then(() => {
      alert("Profile updated successfully.");
      this.profle.saveProfile(this.profile);
      this.navCtrl.push('ProfilePage', {uid: this.uid});
    });
  }

}
