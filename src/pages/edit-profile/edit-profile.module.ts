import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProfile } from './edit-profile';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    EditProfile,
  ],
  imports: [
    IonicPageModule.forChild(EditProfile),
  ],
  exports: [
    EditProfile
  ],
  providers: [
    Profile,
    FirebaseService
  ]
})
export class EditProfileModule {}
