import { Component, NgModule, OnInit } from '@angular/core';
import { IonicPage, App, NavParams, Platform, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import {BarcodeScanner, BarcodeScannerOptions} from '@ionic-native/barcode-scanner';
import { Profile } from '../../providers/profile';
import { Auth } from '../../providers/auth';
import { FirebaseService } from '../../providers/firebase-service';
import { Payment } from '../../providers/payment';

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})

@NgModule({
    imports: [
        //QRCodeModule,
    ]
})

export class Pay {

  options: BarcodeScannerOptions;
  result: any;
  paymentData: string;
  payment: any;
  uid: any;
  qrCode: any;
  paymentForm: any;
  uprofile: any;
  showForm: any;
  showLoading: any;
  showOffline: any;
  loader: any;
  navCtrl: any;
  smartOption: any;
  paymentIntro: any;
  PayNow: any;
  generateQR: any;
  scanQR: any;
  backBtn: any;
  generateQRBtn: any;
  generateQRText: any;

  constructor(public app: App, public platform: Platform, 
    public barcode: BarcodeScanner, public alertCtrl: AlertController, 
    public profile: Profile, public auth: Auth, public pp: Payment,
    public loadr: LoadingController, public fservice: FirebaseService, public asc: ActionSheetController) {    
    
    this.navCtrl = this.app.getActiveNav();
    this.showLoadingSpinner();     

    this.paymentForm = true;
    this.paymentData = "";
    this.payment = {};
    this.result = {};
    this.uprofile = {};
  }

  ngOnInit() {
    let _self = this;
    let timeOut = setTimeout(function(e) {
      _self.showLoading = _self.profile.toFalse();
      _self.showOffLine();
    }, 5000);

    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Processing..."
    });
    
    let preFetch = this.fservice.getCurrentUser()
      .subscribe( (user) => { 
        this.uid = user.uid;
        this.fservice.getSavedProfile()
        .subscribe( (sprofile) => {
          if(sprofile && sprofile.phoneNumber) {
            this.payment.fromSmartPayID = sprofile.phoneNumber;
            this.uprofile.fromSmartPayID = sprofile.phoneNumber;
            this.showIntro();
            clearTimeout(timeOut);
          }
          this.fservice.setTime(3000).subscribe( timeOt => {
            //throw new Error("Processing taking longer than usual.");
            preFetch.unsubscribe();
          })
          this.fservice.getUserProfile(this.uid)
            .take(1)
                      
            .subscribe( (oprofile) => {
              if(oprofile && oprofile.phoneNumber) {
                this.payment.fromSmartPayID = oprofile.phoneNumber;
                this.uprofile.fromSmartPayID = oprofile.phoneNumber;
                this.showIntro();
                clearTimeout(timeOut);
              } else {
                let confm = confirm("You must setup your paybag before making payment. Do you want to set it up now?");
                if(confm) {
                  this.navCtrl.setRoot('SetupPage');
                } else {
                  this.navCtrl.pop();
                }
                
              }
            }, (error) => {
              alert(error);
              return;
            })            
        })
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Pay');
  }

  showMore() {
    let actionSheet = this.asc.create({
      title: 'Shortcuts',
      buttons: [
        { 
          text: 'Settings',
          role: 'destructive',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.navCtrl.push('Options');
            })            
          }
        },{
          text: 'Help',
          'role': 'cancel',
          handler: () => {
            actionSheet.dismiss().then( () => {
              this.navCtrl.push('HelpPage');
            })            
          }
        }
      ]
    });
    actionSheet.present();
  }

  goBack() {
    this.showIntro();
  }
  showOptionForm() {
    let index = parseInt(this.smartOption);
    switch(index) {
      case 0:
        this.showPayNow();
        this.smartOption = "";
        break;
      case 1:
        this.showGenerateQR();
        this.smartOption = "";
        break;
      case 2:
        this.showScanQR();
        this.smartOption = "";
        break;
      default:
        alert("Please select an option");
        this.backBtn = false;
        this.showIntro();
        break;
    }
  }

  showOffLine() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = true;
    this.showLoading = false;
    this.backBtn = false;
    this.qrCode =false;
  }

  showLoadingSpinner() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = true;
    this.backBtn = false;
    this.qrCode =false;
  }

  showPayNow() {
    this.paymentIntro = false;
    this.PayNow = true;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = true;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showGenerateQR() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = true;
    this.generateQRBtn = true;
    this.generateQRText = true;
    this.scanQR = false;
    this.showForm = true;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showScanQR() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = true;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showIntro() {
    this.paymentIntro = true;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = false;
    this.qrCode =false;
  }

  payNow() {
    if(this.pp.isValidData(this.payment)) {
      this.loader.present();
      this.pp.addPayment(this.uid, this.payment)
        .then( (pData) => {
          if(this.loader)
            this.loader.dismiss();
          alert("Payment successful!");
          if(pData)
            this.navCtrl.push('PaymentDetail', {payment: pData});
          return;
        }).catch( (err) => {
          if(this.loader)
            this.loader.dismiss();
          alert(err);
          return;
        });
    } else {
      alert("All fields are required");
      return;
    }
  }

  scanQRCode() {
    this.platform.ready().then(() => {
    this.loader.present();
    this.options = {resultDisplayDuration: 0};
    const results = this.barcode.scan(this.options)
      .then( (result) => {        
        if (result) {
            this.result.Text = result.text;
            this.result.Format = result.format;
            let p = this.result.Text.split("*|*");
            this.payment = {};
            this.payment.uid = this.uid;
            this.payment.fromSmartPayID = p[0];
            this.payment.toSmartPayID = p[1];
            this.payment.amount = p[2];
            this.payment.description = p[3];
            this.payment.transactionID = p[4];
            this.payment.type = p[5];
            if(this.payment.type != 2) {
              this.loader.dismiss();
              alert("Invalid recepient QR code. QR Code must be generated by the recepient.");
              return;
            }
            if(this.payment.fromSmartPayID != this.uprofile.fromSmartPayID) {
              this.loader.dismiss();
              //alert(this.payment.fromSmartPayID + '!=' + this.uprofile.fromSmartPayID);
              alert("This QR Code is not intended for you. Please notify the generator if this is an error");
              this.payment = {};
              return;
            }
            //alert(JSON.stringify(this.payment));
            
            if(this.pp.isValidData(this.payment)) {
              this.pp.addPayment(this.payment.uid, this.payment)
                .then( (pData) => { console.log(pData);
                  if(this.loader)
                    this.loader.dismiss();
                  alert("Payment successful!");
                  if(pData)
                    this.navCtrl.push('PaymentDetail', {payment: pData});
                  return;
                }).catch( (err) => {
                  if(this.loader)
                    this.loader.dismiss();
                  alert(err);
                  this.payment = {};
                  return;
                });
            }
        } else {
          alert(JSON.stringify(result));
        }
      }).catch( (error) => {
        if(this.loader)
          this.loader.dismiss();
        console.log('error when scanning product barcode');
        alert(error);
      });
    });

  }

  scanQRCode1() {
    this.platform.ready().then(() => {
    this.options = {resultDisplayDuration: 0};
    const results = this.barcode.scan(this.options)
      .then( (result) => {
        if (result) {
            this.result.Text = result.text;
            this.result.Format = result.format;
            let p = this.result.Text.split("*|*");
            this.payment = {};
            this.payment.uid = p[0];
            this.payment.fromSmartPayID = p[1];
            this.payment.toSmartPayID = p[2];
            this.payment.amount = p[3];
            this.payment.description = p[4];
            this.payment.transactionID = p[5];
            this.payment.type = p[6];
            if(this.payment.type != 1) {
              alert("Invalid code. QR Code must be generated by the payer");
              return;
            }
            if(this.payment.toSmartPayID != this.uprofile.toSmartPayID) {
              alert(this.payment.toSmartPayID + '!=' + this.uprofile.toSmartPayID);
              alert("This QR Code is not intended for you. Please notify the generator if this is an error");
              this.payment = {};
              return;
            } else {
              return this.profile.getUniqueID(this.payment.fromSmartPayID) 
            }            
        } else {
          alert(JSON.stringify(result));
        }
      }).catch( (error) => {
          console.log('error when scanning product barcode');
          alert(error);
      });

      results.then( (data) => {
        if(data) {
          this.payment.uid = data;
          if(this.pp.isValidData(this.payment)) {
            this.pp.addPayment(this.payment.uid, this.payment)
              .then( (pData) => { console.log(pData);
                alert("Payment successful!");
                if(pData)
                  this.navCtrl.push('PaymentDetail', {payment: pData});
                return;
              }).catch( (err) => {
                alert(err);
                this.payment = {};
                return;
              });
          }
        } else {
          alert("Payer's information not found. Please contact admin or retry.");
          return;
        }
      }).catch( (err) => {
        alert(err);
      })
    });

  }

  generateQRCode() {    
    var transactionID =this.pp.randomString(20); 
    if(this.pp.isValidData(this.payment)) {
      let p = this.uid + '*|*' + this.payment.fromSmartPayID + '*|*' + this.payment.toSmartPayID + '*|*' + this.payment.amount + '*|*' + this.payment.description + '*|*' + transactionID + '*|*1';
      this.paymentData = p;
      console.log(p);
      this.qrCode = true;
      this.generateQRBtn = false;
      this.generateQRText = false;
      this.showForm = false;
    } else {
      alert("Some required fields are missing");
      this.qrCode =false;
      this.generateQRBtn = false;
      this.generateQRText = false;
      this.showForm = true;
    }
  }

  confirmPIN(type) {
    /*if(!this.pp.isValidData(this.payment)) {
      alert("All fields are required!");
      return;
    }*/
    let alertConfirm = this.alertCtrl.create({
      title: 'Confirm payment',
      inputs: [
        {
          name: 'pin',
          placeholder: 'Enter PIN',
          type: 'number',

        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            alertConfirm.dismiss();
            return false;
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            this.profile.getPaybag(this.uid)
              .then( (valid) => {
                if(valid.pin != data.pin) {
                  alert("Invalid PIN");
                  return false;
                } else {
                  switch(type) {
                    case 1: 
                      this.payNow();
                      break;
                    case 2:
                      this.scanQRCode();
                      break;
                    default: 
                      break;
                  }                  
                }
              })             
          }
        }
      ]
    });
    alertConfirm.present();
  }

  toHome() {
    this.navCtrl.push('HomePage');
  }

  refreshPage() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

}
