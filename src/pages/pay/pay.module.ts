import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Pay } from './pay';
import { QRCodeModule } from 'angular2-qrcode';
import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    Pay,
  ],
  imports: [
    IonicPageModule.forChild(Pay),
    QRCodeModule
  ],
  exports: [
    Pay
  ],
  providers: [
    BarcodeScanner,
    FirebaseService
  ]
})
export class PayModule {}
