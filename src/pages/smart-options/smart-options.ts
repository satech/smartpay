import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SmartOptions page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-smart-options',
  templateUrl: 'smart-options.html',
})
export class SmartOptions {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SmartOptions');
  }

  goTo(pos) {
    console.log(pos);
    let index = parseInt(pos);

    switch(index) {
      case 0:
        this.navCtrl.push('Pay');
        break; 
      case 1:
        this.navCtrl.push('Receive');
        break;
      case 2:
        this.navCtrl.push('Transactions');
        break;
      case 3:
        this.navCtrl.push('Paybag');
        break;
      case 4:
        this.navCtrl.push('LoadPaybag');
        break;
      case 5:
        this.navCtrl.push('Payout');
        break;
      default:
        break;
    };
  }

}
