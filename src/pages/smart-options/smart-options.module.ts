import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmartOptions } from './smart-options';

@NgModule({
  declarations: [
    SmartOptions,
  ],
  imports: [
    IonicPageModule.forChild(SmartOptions),
  ],
  exports: [
    SmartOptions
  ]
})
export class SmartOptionsModule {}
