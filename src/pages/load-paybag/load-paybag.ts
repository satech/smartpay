import { Component } from '@angular/core';
import { App, IonicPage, LoadingController } from 'ionic-angular';
import { FirebaseService } from '../../providers/firebase-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the LoadPaybag page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-load-paybag',
  templateUrl: 'load-paybag.html',
})
export class LoadPaybag {

  months: any;
  years: any;
	showLoading: any;
	showOffline: any;
	chooseMethod: any;
	cardDetail: any;
	previewDetail: any;
	bankPreview: any;
	cardPreview: any;
	bankDetail: any;
	card: any;
	bank: any;
	navCtrl: any;
	amount: any;
	uid: any;
	method:any;
	system: any;
	token: any;
	enterCardOTP: any;
	enterBankOTP: any;
	loading: any;

  constructor(public app: App, public fservice: FirebaseService, public iab: InAppBrowser, public loader: LoadingController) {
    this.months = this.loadMonths();
    this.years = this.loadYears();
		this.navCtrl = this.app.getActiveNav();
		this.card = {};
		this.bank = {};
		this.method = '';
		this.amount = '';
		this.system = {};
		this.token = '';
		//this.loading = this.fservice.presentLoading(this);

		let _self = this;
    let timeOut = setTimeout(function(e) {
      _self.ShowOffline();
    }, 5000);

		this.fservice.authMoneywave()
      .subscribe( (auth) => {
        if(auth && auth.status == "success") {
					this.token = auth.token;
				}		
    });

		this.fservice.getSystemDefault()
			.subscribe( (d) => {
				this.system = d;
			});

		let preFetch = this.fservice.getCurrentUser()
      .subscribe( (suser) => {
        this.uid = suser.uid;
				this.fservice.getSavedAccount()
					.subscribe( (acct) => {
						if(acct) {
							this.bank.sender_account_number = acct.number;
							this.bank.sender_bank = acct.bankID;
							this.bank.sender_bankName = acct.bankName;
						}
					});
        this.fservice.getSavedProfile()				
          .subscribe( (sprofile) => {
            if(sprofile && sprofile.phoneNumber) {
              this.card.phonenumber = sprofile.phoneNumber;
              this.card.firstname = sprofile.firstName;
							this.card.lastname = sprofile.surname;
							this.card.email = suser.email;
							this.bank.phonenumber = sprofile.phoneNumber;
              this.bank.firstname = sprofile.firstName;
							this.bank.lastname = sprofile.surname;
							this.bank.email = suser.email;							
              this.showChooseMethod();
              clearTimeout(timeOut);
            } 
						this.fservice.getUserAccount(this.uid)
						.subscribe( (oacct) => {
							console.log(oacct);
						
				this.fservice.getUserProfile(this.uid)
              .subscribe( (oprofile) => {
                if(oprofile && oprofile.phoneNumber) {
                  this.card.phonenumber = oprofile.phoneNumber;
									this.card.firstname = oprofile.firstName;
									this.card.lastname = oprofile.surname;
									this.card.email = suser.email;
									this.bank.phonenumber = oprofile.phoneNumber;
									this.bank.firstname = oprofile.firstName;
									this.bank.lastname = oprofile.surname;
									this.bank.email = suser.email;
									if(oacct) {
										this.bank.sender_account_number = oacct.number;
										this.bank.sender_bank = oacct.bankID;
										this.bank.sender_bankName = oacct.bankName;
									}
									this.showChooseMethod();
                  clearTimeout(timeOut);
                } else {
                  let confm = confirm("You must setup your paybag before making payment. Do you want to set it up now?");
                  if(confm) {
                    this.navCtrl.setRoot('SetupPage');
                  } else {
                    this.navCtrl.pop();
                  }
                }
              });
            
              this.fservice.setTime(5000).subscribe( timeOt => {
                preFetch.unsubscribe();
              });
          })
					})
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoadPaybag');
  }

  loadMonths() {
    let months = [];
    let monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December' ];
    for(let i = 1; i < 13; i++) {
      if(i < 10) {
        months.push({id: "0"+i, name: monthNames[i]});
      } else {
        months.push({id: i, name: monthNames[i]});
      }
    }
    return months;
  }

  loadYears() {
    let year = (new Date()).getFullYear();
    let years = [];
    for(let i = year - 5; i < year + 20;i ++) {
      years.push(i);
    }
		return years;
  }

  goToStepForward(step) {
	  let index = parseInt(step);
	  switch(index) {
		case 0:
			this.showChooseMethod();
			break;
		case 1:
			if(this.isMethodChosen())
				this.showCardDetail();
			else
				alert("Choose a language");
			break;
		case 2:
			if(this.isCardValid())
				this.showCardPreview();
			else
				alert("Enter required profile fields");
			break;
		default:
			this.showChooseMethod();
			break;
	  }
  }
  
  goToStepBack(step) {
	  let index = parseInt(step);
	  switch(index) {
		case 0:
		  	this.showChooseMethod();
			break;
		case 1:
			this.showCardDetail();
			break;
		case 2:
			this.showCardPreview();
			break;
		default:
			this.showChooseMethod();
			break;
	  }
  }

  ShowLoading() {
		this.showLoading = true;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	ShowOffline() {
		this.showLoading = false;
		this.showOffline = true;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showChooseMethod() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = true;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showCardDetail() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = true;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showCardPreview() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = true;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showBankDetail() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = true;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showBankPreview() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = true;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = false;
  }

	showEnterCardOTP() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = true;
		this.enterBankOTP = false;
  }

	showEnterBankOTP() {
		this.showLoading = false;
		this.showOffline = false;
		this.chooseMethod = false;
		this.cardDetail = false;
		this.bankDetail = false;
		this.bankPreview = false;
		this.cardPreview = false;
		this.enterCardOTP = false;
		this.enterBankOTP = true;
  }

	selectNext() { if(this.isMethodChosen()){ (this.method == '0')? this.showCardDetail(): this.showBankPreview(); } }

	isMethodChosen() { if(this.method && this.amount){ return true; } else { alert("Please enter amount and select an option below."); return false; } }

	isCardValid() { if(this.card.card_no && this.card.cvv && this.card.cvv && this.card.pin && this.card.expiry_year && this.card.expiry_month){ return true; } else { return false; } }
	isBankValid() { return true; }
	toShowCardPreview() {
		if(this.isCardValid) {
			this.showCardPreview();
		} else {
			alert("Required card fields missing");
			return;
		}
	}

	transferFromBank() {
		if(this.isBankValid()) {
			this.bank.recipient = "account";
			this.bank.charge_with = "account";
			this.bank.email = "rasheedsaidi@gmail.com";
			this.bank.recipient_bank = this.system.bankCode;
			this.bank.recipient_account_number = this.system.accountNumber;
			this.bank.amount = this.amount;
			this.bank.naration = "Transfer of " + this.amount + " from " + this.bank.sender_account_number + " Paybag";
			this.bank.fee = this.system.fee;
			this.bank.medium = "mobile";
			this.bank.redirectUrl = this.system.redirectUrl;
			console.log(this.bank)
			this.loading = this.fservice.presentLoading(this);
			this.fservice.accountToAccountTransfer(this.token, this.bank)
				.subscribe( (result) => {
					this.fservice.dismissLoading(this.loading);
					console.log(result);
					this.bank.ref = result.data.transfer.flutterChargeReference;
					this.showEnterBankOTP();
				}, (error) => {
					this.fservice.dismissLoading(this.loading);
					alert(error)
				})
		} else {
			alert("Required bank info missing");
			return;
		}
	}

	transferByCard() {
		if(this.isCardValid()) {
			this.card.recipient = "account";
			this.card.charge_with = "account";
			this.card.email = "rasheedsaidi@gmail.com";
			this.card.recipient_bank = this.system.bankCode;
			this.card.recipient_account_number = this.system.accountNumber;
			this.card.amount = this.amount;
			this.card.naration = "Transfer of " + this.amount + " from " + this.card.sender_account_number + " Paybag";
			this.card.fee = this.system.fee;
			this.card.medium = "mobile";
			this.card.redirectUrl = this.system.redirectUrl;
			
			this.loading = this.fservice.presentLoading(this);
			console.log(this.card);
			this.fservice.cardToAccountTransfer(this.token, this.card)
				.subscribe( (result) => {
					this.fservice.dismissLoading(this.loading);
					console.log(result);
					if(result && result.status == 'success') {
						let url = result.data.authurl;
						this.card.ref = result.data.transfer.flutterChargeReference;
						const browser = this.iab.create(url);
						this.showEnterCardOTP();
					}
				}, (error) => {
					this.fservice.dismissLoading(this.loading);
					alert(error)
				})
		} else {
			alert("Required card info missing");
			return;
		}
	}

	confirmCardOTP() {
		if(this.card.otp) {
			this.loading = this.fservice.presentLoading(this);
			console.log({transactionRef: this.card.ref, otp: this.card.otp});
			this.fservice.cardToAccountValidation(this.token, {transactionRef: this.card.ref, otp: this.card.otp})
				.subscribe( (result) => {
					this.fservice.dismissLoading(this.loading);
					console.log(result);
					if(result && result.status == 'success') {
						//this.showEnterOTP();
					}
				}, (error) => {
					this.fservice.dismissLoading(this.loading);
					alert(error)
				})
		} else {
			alert("Required card info missing");
			return;
		}
	}

	confirmBankOTP() {
		if(this.bank.otp) {
			this.loading = this.fservice.presentLoading(this);
			this.fservice.accountToAccountValidation(this.token, {transactionRef: this.bank.ref, authType: 'OTP', authValue: this.bank.otp})
				.subscribe( (result) => {
					this.fservice.dismissLoading(this.loading);
					console.log(result);
					if(result && result.status == 'success') {
						let msg = result.data.flutterDisburseResponseMessage;
						alert(msg);
						//this.showCardSuccess();

					}
				}, (error) => {
					this.fservice.dismissLoading(this.loading);
					alert(error)
				})
		} else {
			alert("Required bank info missing");
			return;
		}
	}

	refreshPage() {
    //let index = this.navCtrl.getActive().index;
    let page = this.navCtrl.getActive().component;
    this.navCtrl.push('LoadPaybag');
    let _self = this;
    setTimeout(function() {
      _self.navCtrl.pop(page);
    })
  }

	toProfile() {
		this.navCtrl.push('ProfilePage');
	}
}
