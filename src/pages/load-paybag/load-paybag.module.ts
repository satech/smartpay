import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoadPaybag } from './load-paybag';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    LoadPaybag,
  ],
  imports: [
    IonicPageModule.forChild(LoadPaybag),
  ],
  exports: [
    LoadPaybag
  ],
  providers: [
    FirebaseService
  ]
})
export class LoadPaybagModule {}
