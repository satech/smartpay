import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentDetail } from './payment-detail';

@NgModule({
  declarations: [
    PaymentDetail,
  ],
  imports: [
    IonicPageModule.forChild(PaymentDetail),
  ],
  exports: [
    PaymentDetail
  ]
})
export class PaymentDetailModule {}
