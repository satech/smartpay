import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Transactions } from '../transactions/transactions';

/**
 * Generated class for the PaymentDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-payment-detail',
  templateUrl: 'payment-detail.html',
})
export class PaymentDetail {

  payment: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let p = this.navParams.get('payment');
    this.payment = (p)? p: {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentDetail');
  }

  toHomePage() {
    this.navCtrl.push(HomePage);
  }

  toTransactions() {
    this.navCtrl.push(Transactions);
  }

}
