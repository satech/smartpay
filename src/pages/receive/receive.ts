import { Component, NgModule, OnInit } from '@angular/core';
import { IonicPage, App, NavController, NavParams, Platform, AlertController, LoadingController } from 'ionic-angular';
import { QRCodeModule } from 'angular2-qrcode';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';
import { Auth } from '../../providers/auth';
import { Payment } from '../../providers/payment';

/**
 * Generated class for the Pay page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-receive',
  templateUrl: 'receive.html',
})

@NgModule({
    imports: [
        QRCodeModule,
    ]
})

export class Receive {

  options: BarcodeScannerOptions;
  result: any;
  paymentData: string;
  payment: any;
  uid: any;
  qrCode: any;
  showForm: any;
  showLoading: any;
  showOffline: any;
  uprofile: any;
  loader: any;
  navCtrl: any;
  smartOption: any;
  paymentIntro: any;
  PayNow: any;
  generateQR: any;
  scanQR: any;
  backBtn: any;
  generateQRBtn: any;
  generateQRText: any;
  paymentForm:any;


  constructor(public app: App, public platform: Platform, 
    public barcode: BarcodeScanner, public alertCtrl: AlertController, public profile: Profile, public auth: Auth, 
    public pp: Payment, public loadr: LoadingController, public fservice: FirebaseService) {    
    
    this.navCtrl = this.app.getActiveNav();
    this.showLoadingSpinner();     

    this.paymentForm = true;
    this.showForm = false;
    this.showLoading = true;
    this.showOffline = false;
    this.paymentData = "";
    this.payment = {};
    this.result = {};
    this.uprofile = {};
  }

  ngOnInit() {
    let _self = this;
    let timeOut = setTimeout(function(e) {
      _self.showLoading = _self.profile.toFalse();
      _self.showOffline = _self.profile.toTrue();
    }, 5000);

    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Processing..."
    });
    
    let preFetch = this.fservice.getCurrentUser()
      .subscribe( (suser) => {
        this.uid = suser.uid;
        this.fservice.getSavedProfile()
          .subscribe( (sprofile) => {
            if(sprofile && sprofile.phoneNumber) {
              this.payment.toSmartPayID = sprofile.phoneNumber;
              this.uprofile.toSmartPayID = sprofile.phoneNumber;
              this.showIntro();
              clearTimeout(timeOut);
            }
            
            this.fservice.getUserProfile(this.uid)
              .subscribe( (oprofile) => {
                if(oprofile && oprofile.phoneNumber) {
                  this.payment.toSmartPayID = oprofile.phoneNumber;
                  this.uprofile.toSmartPayID = oprofile.phoneNumber;
                  this.showIntro();
                  clearTimeout(timeOut);
                } else {
                  let confm = confirm("You must setup your paybag before making payment. Do you want to set it up now?");
                  if(confm) {
                    this.navCtrl.setRoot('SetupPage');
                  } else {
                    this.navCtrl.pop();
                  }
                }
              });
            
              this.fservice.setTime(5000).subscribe( timeOt => {
                preFetch.unsubscribe();
              });
          })
      })  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Pay');
  }

  showOptionForm() {
    let index = parseInt(this.smartOption);
    switch(index) {
      case 0:
        this.showGenerateQR();
        this.smartOption = "";
        break;
      case 1:
        this.showScanQR();
        this.smartOption = "";
        break;
      default:
        alert("Please select an option");
        this.backBtn = false;
        this.showIntro();
        break;
    }
  }

  showOffLine() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = true;
    this.showLoading = false;
    this.backBtn = false;
    this.qrCode =false;
  }

  showLoadingSpinner() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = true;
    this.backBtn = false;
    this.qrCode =false;
  }

  showPayNow() {
    this.paymentIntro = false;
    this.PayNow = true;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = true;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showGenerateQR() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = true;
    this.generateQRBtn = true;
    this.generateQRText = true;
    this.scanQR = false;
    this.showForm = true;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showScanQR() {
    this.paymentIntro = false;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = true;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = true;
    this.qrCode =false;
  }

  showIntro() {
    this.paymentIntro = true;
    this.PayNow = false;
    this.generateQR = false;
    this.generateQRBtn = false;
    this.generateQRText = false;
    this.scanQR = false;
    this.showForm = false;
    this.showOffline = false;
    this.showLoading = false;
    this.backBtn = false;
    this.qrCode =false;
  }

  payNow() {
    if(this.pp.isValidData(this.payment)) {
      this.loader.present();
      this.pp.addPayment(this.uid, this.payment)
        .then( (pData) => {
          this.loader.dismiss();
          alert("Payment successful!");
          if(pData)
            this.navCtrl.push('PaymentDetail', {payment: pData});
          return;
        }).catch( (err) => {
          this.loader.dismiss();
          alert(err);
          return;
        });
    } else {
      alert("All fields are required");
      return;
    }
  }

  scanQRCode() {
    this.platform.ready().then(() => {
    this.loader.present();
    this.options = {resultDisplayDuration: 0};
    const results = this.barcode.scan(this.options)
      .then( (result) => {
        if (result) {
            this.result.Text = result.text;
            this.result.Format = result.format;
            let p = this.result.Text.split("*|*");
            this.payment = {};
            this.payment.uid = p[0];
            this.payment.fromSmartPayID = p[1];
            this.payment.toSmartPayID = p[2];
            this.payment.amount = p[3];
            this.payment.description = p[4];
            this.payment.transactionID = p[5];
            this.payment.type = p[6];
            if(this.payment.type != 1) {
              this.loader.dismiss();
              alert("Invalid payer QR code. QR Code must be generated by the payer");
              return;
            }
            if(this.payment.toSmartPayID != this.uprofile.toSmartPayID) {
              //alert(this.payment.toSmartPayID + '!=' + this.uprofile.fromSmartPayID);
              this.loader.dismiss();
              alert("This QR Code is not intended for you. Please notify the generator if this is an error");
              this.payment = {};
              return;
            }
            //alert(JSON.stringify(this.payment));
            if(this.pp.isValidData(this.payment)) {
              this.pp.addPayment(this.payment.uid, this.payment)
                .then( (pData) => { console.log(pData);
                  this.loader.dismiss();                  
                  alert("Payment successful!");
                  if(pData)
                    this.navCtrl.push('PaymentDetail', {payment: pData});
                  return;
                }).catch( (err) => {
                  this.loader.dismiss();
                  alert(err);
                  this.payment = {};
                  return;
                });
            }
        } else {
          alert(JSON.stringify(result));
        }
      }).catch( (error) => {
        this.loader.dismiss();
          console.log('error when scanning product barcode');
          alert(error);
      });
    });

  }
  

  generateQRCode() {    
    var transactionID =this.pp.randomString(20);    
    if(this.pp.isValidData(this.payment)) {
      let p = this.payment.fromSmartPayID + '*|*' + this.payment.toSmartPayID + '*|*' + this.payment.amount + '*|*' + this.payment.description + '*|*' + transactionID + '*|*2';
      this.paymentData = p;
      console.log(p);
      this.qrCode = true;
      this.generateQRBtn = false;
      this.generateQRText = false;
      this.showForm = false;
    } else {
      alert("Some required fields are missing");
      this.qrCode =false;
      this.generateQRBtn = false;
      this.generateQRText = false;
      this.showForm = true;      
    }
  }

  confirmPIN(type) {
    /*if(!this.pp.isValidData(this.payment)) {
      alert("All fields are required!");
      return;
    }*/
    let alertConfirm = this.alertCtrl.create({
      title: 'Confirm payment',
      inputs: [
        {
          name: 'pin',
          placeholder: 'Enter PIN',
          type: 'number',

        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            alertConfirm.dismiss();
            return false;
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            this.profile.getPaybag(this.uid)
              .then( (valid) => {
                if(valid.pin != data.pin) {
                  alert("Invalid PIN");
                  return false;
                } else {
                  switch(type) {
                    case 1:
                      this.scanQRCode();
                      break;
                    default: 
                      break;
                  }                  
                }
              })             
          }
        }
      ]
    });
    alertConfirm.present();
  }

  toHome() {
    this.navCtrl.push('HomePage');
  }

  goBack() {
    this.showIntro();
  }

  refreshPage() {
    let index = this.navCtrl.getActive().index;
    let page = this.navCtrl.getActive().component;
    this.navCtrl.push(page);
    let _self = this;
    setTimeout(function() {
      _self.navCtrl.pop(page);
    })
  }

}
