import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Receive } from './receive';
import { QRCodeModule } from 'angular2-qrcode';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    Receive,
  ],
  imports: [
    IonicPageModule.forChild(Receive),
    QRCodeModule
  ],
  exports: [
    Receive
  ],
  providers: [
    BarcodeScanner,
    FirebaseService
  ]
})
export class ReceiveModule {}
