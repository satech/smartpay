import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Paybag } from './paybag';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    Paybag,
  ],
  imports: [
    IonicPageModule.forChild(Paybag),
  ],
  exports: [
    Paybag
  ],
  providers: [
    FirebaseService
  ]
})
export class PaybagModule {}
