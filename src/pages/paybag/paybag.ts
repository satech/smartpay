import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Auth } from '../../providers/auth';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-paybag',
  templateUrl: 'paybag.html',
})
export class Paybag {
  paybag: any;  //FirebaseObjectObservable<any>;
  showPaybag: any;
  showLoading: any;
  showOffline: any;
  uid: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase, 
    public auth: Auth, public profile: Profile, public fservice: FirebaseService) {
	
    this.paybag = FirebaseObjectObservable.create();
    this.showPaybag = false;
    this.showLoading = true;
    this.showOffline = false;
  
    let _self = this;
    let timeOut = setTimeout(function(e) {
      console.log('Got here');
      _self.showLoading = false;
      if(_self.paybag && _self.paybag.balance) {
        _self.showPaybag = true;
        _self.showOffline = false;
      } else {
        _self.showPaybag = false;
        _self.showOffline = true;
      }
    }, 5000);
    
    this.fservice.getCurrentUser()
      .take(1)
      .subscribe( (user) => {
        this.uid = user.uid;
        this.fservice.getSavedPaybag()
          .take(1)
          .subscribe( (spaybag) => {
            console.log(spaybag);
            console.log(timeOut)
            clearTimeout(timeOut);
            if(spaybag && spaybag.balance) {
              this.paybag = spaybag;
              this.showPaybag = true;
              this.showLoading = false;
              this.showOffline = false;              
            }

            this.fservice.getUserPaybag(this.uid)
              .take(1)
              .subscribe( (opaybag) => {
                console.log(opaybag);
                clearTimeout(timeOut);            
                if(opaybag && opaybag.balance) {
                  console.log("1");
                  this.paybag = opaybag;
                  this.showPaybag = true;
                  this.showLoading = false;
                  this.showOffline = false;
                } else {
                  console.log("2");
                  let confm = confirm("You must setup your paybag before making payment. Do you want to set it up now?");
                  if(confm) {
                    this.navCtrl.setRoot('SetupPage');
                  } else {
                    this.navCtrl.pop();
                  }
                }
              })
          })
      });
    
  }

  ionViewDidLoad() {  }

  toLoadPaybag() {
    this.navCtrl.push('LoadPaybag');
  }

  toPayout() {
    this.navCtrl.push('Payout');
  }
  
  toTransactions() {
    this.navCtrl.push('Transactions');
  }

  refreshPage() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

}
