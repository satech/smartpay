import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Auth } from '../../providers/auth';


@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {
  navCtrl: any;
  constructor(public app: App, public auth: Auth) {
    this.auth.setSeen();
    this.navCtrl = this.app.getActiveNav();
  }

  ionViewDidLoad() { }

  toLoginPage() {
    this.navCtrl.push('LoginPage');
  }

}
