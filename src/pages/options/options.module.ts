import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { SmartpayTranslateLoader } from '../../providers/smartpay-translate-loader';
import { Http } from '@angular/http';
import { Options } from './options';
import { OptionsService } from '../../providers/options-service';

@NgModule({
  declarations: [
    Options,
  ],
  imports: [
    IonicPageModule.forChild(Options),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (SmartpayTranslateLoader),
        deps: [Http]
      }
    })
  ],
  exports: [
    Options
  ],
  providers: [
    OptionsService
  ]
})
export class OptionsModule {}
