import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OptionsService } from '../../providers/options-service';
import {TranslateService} from '@ngx-translate/core';

/**
 * Generated class for the Options page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class Options {

  /*static get parameters() {
    return [[OptionsService]];
  } */

  settings: any;
  PREF_LANGUAGE: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public option: OptionsService, public translate: TranslateService) {
    //this.option = OptionsService;
    this.PREF_LANGUAGE = 'pref_language';//this.option.PREF_LANGUAGE;
    this.settings = {};
    this.translate.setDefaultLang('en');
    this.translate.use('en');
    this.option.getPreferences().then( (setting) => {
      console.log('Setting:', setting);
      this.settings = JSON.parse(setting);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Options');
  }

  changePreference(event, key){
    console.log(event);
    this.option.setPreference(key, event);
  }

}
