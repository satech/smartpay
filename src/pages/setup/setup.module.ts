import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetupPage } from './setup';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    SetupPage,
  ],
  imports: [
    IonicPageModule.forChild(SetupPage),
  ],
  exports: [
    SetupPage
  ],
  providers: [
	FirebaseService
  ]
})
export class SetupModule {}
