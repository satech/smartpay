import { Component } from '@angular/core';
import { App, IonicPage, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';
import 'rxjs/add/operator/timeout';

/**
 * Generated class for the Setup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
})
export class SetupPage {

  uid: any = null;
  fullName: any;
  phoneNumber: any;
  gender: any;
  accountName: any;
  accountNumber: any;
  bankName: any;
  address: any;
  pin: any;
  confirmPIN: any;
  proceed: any = false;
  showForm: any;
  showLoading: any;
  showOffline: any;
  chooseLanguage: any;
  createProfile: any;
  addAccount: any;
  securePaybag: any;
  account:any;
  profile: any;
  paybag: any;
  navCtrl: any;
  banks: Array<any>;
  loader: any;
  token: any;

  constructor(public app: App, public profileProvider: Profile, public auth: Auth, public fservice: FirebaseService, public loadr: LoadingController) {
	this.navCtrl = this.app.getActiveNav();	
	this.account = {};
	this.profile = {};
	this.paybag = {};
	this.banks = this.fservice.getBanks();
	this.confirmPIN = '';
	this.token = '';
	this.showChooseLanguage();
	this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "processing ..."
    });
	let _self = this;
	let timeOut = setTimeout(function(e) {
		_self.showLoading = _self.profileProvider.toFalse();
		if(this.profile && this.paybag.balance) {
			_self.showChooseLanguage();
		} else {
			_self.ShowOffline();
		}
	}, 5000);
	
	this.fservice.authMoneywave()
      .subscribe( (auth) => {
	  console.log(auth)
        if(auth && auth.status == "success") {
			this.token = auth.token;
		}
		
    });
	
	this.fservice.getCurrentUser()
      .subscribe( (user) => {
		this.uid = user.uid
		this.fservice.getUserProfile(this.uid)
			.take(1)
		  .subscribe( (sprofile) => {
			clearTimeout(timeOut);
			if(sprofile && sprofile.phoneNumber) {
			  alert("The current User has already setup a Paybag. Please login or create a new account.");
			  this.navCtrl.push('HomePage');			  
			} else {
			  this.showChooseLanguage();
			}
		  })
	  });
	  
  	/*this.auth.getUser().then((u) => {
      let user = JSON.parse(u);
      this.uid = user.uid;
	  return this.profileProvider.getUserProfile(user.uid);
    }).then( (uprofile) => {
		clearTimeout(timeOut);
		if(uprofile && uprofile.phoneNumber) {
			alert("The current User has already setup a Paybag. Please login or create a new account.");
			this.navCtrl.push('LoginPage');
		} else {
			this.showForm = true;
			this.showLoading = false;			
			this.showOffline = false;
		}
	})*/
  }

  refreshPage() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Setup');
  }

  ShowLoading() {
	this.showLoading = true;
	this.showOffline = false;
	this.chooseLanguage = false;
	this.createProfile = false;
	this.addAccount = false;
	this.securePaybag = false;
  }
  
  ShowOffline() {
	this.showLoading = false;
	this.showOffline = true;
	this.chooseLanguage = false;
	this.createProfile = false;
	this.addAccount = false;
	this.securePaybag = false;
  }

  showChooseLanguage() {
	this.showLoading = false;
	this.showOffline = false;
	this.chooseLanguage = true;
	this.createProfile = false;
	this.addAccount = false;
	this.securePaybag = false;
  }

  showCreateProfile() {
	this.showLoading = false;
	this.showOffline = false;
	this.chooseLanguage = false;
	this.createProfile = true;
	this.addAccount = false;
	this.securePaybag = false;
  }

  showAddAccount() {
	this.showLoading = false;
	this.showOffline = false;
	this.chooseLanguage = false;
	this.createProfile = false;
	this.addAccount = true;
	this.securePaybag = false;
  }

  showSecurePaybag() {
	this.showLoading = false;
	this.showOffline = false;
	this.chooseLanguage = false;
	this.createProfile = false;
	this.addAccount = false;
	this.securePaybag = true;
  }

  goToStepForward(step) {
	  let index = parseInt(step);
	  switch(index) {
		case 0:
			this.showChooseLanguage();
			break;
		case 1:
			if(this.isLanguageSet())
				this.showCreateProfile();
			else
				alert("Choose a language");
			break;
		case 2:
			if(this.isProfileValid())
				this.showAddAccount();
			else
				alert("Enter required profile fields");
			break;
		case 3:
			if(this.isAccountValid()) {
				this.validateAccount()
			}				
			else
				alert("Enter required account fields");
			break;
		default:
			this.showChooseLanguage();
			break;
	  }
  }
  
  goToStepBack(step) {
	  let index = parseInt(step);
	  switch(index) {
		case 0:
		  	this.showChooseLanguage();
			break;
		case 1:
			this.showCreateProfile();
			break;
		case 2:
			this.showAddAccount();
			break;
		case 3:
			this.showSecurePaybag();
			break;
		default:
			this.showChooseLanguage();
			break;
	  }
  }
  
  isLanguageSet() { if(this.profile.pref_language){ return true; } else { return false; } }
  isProfileValid() { if(this.profile.firstName && this.profile.surname && this.profile.gender && this.profile.address && this.profile.phoneNumber){ return true; } else { return false; } }

  isAccountValid() { 
		if(this.account.firstName && this.account.surname && this.account.number && this.account.bankID){
			return true;
		} else {
			return false; 
		}
	}

	validateAccount() {
		let verifyAcct = this.fservice.validateAccount(this.token, {account_number: this.account.number, bank_code: this.account.bankID})
		  .take(1)
			.timeout(5000)
			.subscribe( (result) => {
				console.log(result);
				if(result.status == "success" && result.data) {
					this.account.apiAccountName = result.data.account_name;
					this.showSecurePaybag();
				} else {
					alert("Account not verified. Please retry or contact admin.");
					return false;
				}
		  }, (error) => {
				verifyAcct.unsubscribe();
				alert(error)
				console.log(error);
				return false;
		  });
  }
  isPasswordsSame() { return this.paybag.pin === this.confirmPIN; }

  setup() {
  	if(!this.uid) {
  		alert("User selected is invalid. Please go back and retry or contact admin");  		
  	}
  	if(!this.isLanguageSet()) {
  		alert("Language was not selected.");
  		return;
  	}
	
	if(!this.isProfileValid()) {
  		alert("Required profile information is missing.");
  		return;
  	}
	
	if(!this.isAccountValid()) {
  		alert("Required account information is missing.");
  		return;
  	}
	
	if(!this.isPasswordsSame()) {
  		alert("PIN confirmation does not match.");
  		return;
  	}
	
	if(this.account.bankID) {
		let bname = this.fservice.getBankByID(this.account.bankID);		
		if(bname) {
			this.account.bankName = bname;
		}
	}	
  
	let bag = {transaction: 10, balance: 10, prevBalance: 0, pin: this.paybag.pin};
	console.log(this.account);
		//this.loader.present();
  	this.profileProvider.createProfile(this.uid, this.profile, bag)
		.then( () => { return this.profileProvider.addAccount(this.uid, this.account) } )
  		.then( () => { return this.profileProvider.getUniqueID(this.profile.phoneNumber); } )
		.then( (id) => {
			console.log("ret: " + id)
			if(!id) {
				this.proceed = true;
				return this.profileProvider.addUniqueID(this.uid, this.profile.phoneNumber);				
			} else {
				alert('The phone number is already in use. Login instead if you\'ve registered or reset your password.');
				return false;
			}
		})
		.then( (resp) => {
			console.log(resp);
			if(this.proceed) {
				alert('Paybag setup successful!');
				this.auth.loggedIn()
					.subscribe( (user) => {
						if(!user) {
							this.navCtrl.setRoot('LoginPage');
						} else {
							this.navCtrl.setRoot('HomePage');
						}
					})				
			} else {

			}
			
			
		}).catch ( (err) => {
			console.log(err);
			alert("Unknown error occured. Please retry.");
		});
  }

}
