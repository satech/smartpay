import { Component } from '@angular/core';
import { App, LoadingController, IonicPage, AlertController, ActionSheetController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Auth } from '../../providers/auth';
import {TranslateService} from '@ngx-translate/core';
/*import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';
import { Pay } from '../pay/pay';
import { Receive } from '../receive/receive';
import { Paybag } from '../paybag/paybag';*/
import { Transactions } from '../transactions/transactions';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loader: any;
  uid: any;
  navCtrl: any;
  constructor(public app: App, public storage: Storage, public auth: Auth, 
  public loadr: LoadingController, public translate: TranslateService, public alertCtrl: AlertController,
    public asc: ActionSheetController) {

    this.navCtrl = this.app.getActiveNav();
    this.translate.setDefaultLang('en');
    this.translate.use('en');

    this.auth.getUser().then((u) => {
      let user = JSON.parse(u);
      this.uid = user.uid;
    })
    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Signing out..."
    });
  }

  setSeen() {
    console.log("set seen called");
    this.storage.set('startSeen', 1);
  }

  clearSeen() {
    console.log("clear seen called");
    this.storage.remove('startSeen');
  }

  logout() {
    if(this.loader)
      this.loader.present();
    this.auth.logout()
    .then(() => {
      this.storage.remove('currentuser');
      this.storage.remove('profile');
      this.storage.remove('paybag');
      this.storage.remove('transactions');
      if(this.loader)
        this.loader.dismiss();
      this.navCtrl.push('LoginPage');
    })
    
  }

  showMore() {
    let _self = this;
    let actionSheet = this.asc.create({
      title: 'Shortcuts',
      buttons: [
        { 
          text: 'Settings',
          //role: 'destructive',
          handler: () => {
            actionSheet.dismiss().then( () => {
              _self.navCtrl.push('Options');
            });
            return false;         
          }
        },{
          text: 'Help',
          handler: () => {
            actionSheet.dismiss().then( () => {
              _self.navCtrl.push('Help');
            });
            return false;        
          }
        },{
          text: 'Logout',
          handler: () => {
            actionSheet.dismiss().then( () => {
              _self.logout();
            });
            return false;      
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            actionSheet.dismiss().then( () => { });
            return false;           
          }
        }
      ]
    });
    actionSheet.present();
  }

  toProfilePage() {
    this.navCtrl.push('ProfilePage', {uid: this.uid});
  }

  toPay() {
    this.navCtrl.push('Pay');
  }

  toTransactions() {
    this.navCtrl.push('Transactions');
  }

  toPaybag() {
    this.navCtrl.push('Paybag');
  }

  toReceive() {
    this.navCtrl.push('Receive');
  }

  toProfile() {
    this.navCtrl.push('ProfilePage');
  }

  toHelpPage() {
    this.navCtrl.push('Help');
  }

  toSmartOptions() {
    this.navCtrl.push('SmartOptions');
  }

  toLoadPaybag() {
    this.navCtrl.push('LoadPaybag');
  }

  toPayout() {
    this.navCtrl.push('Payout');
  }

  showSmartOptions(type) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Smart options');

    alert.addInput({
      type: 'radio',
      label: 'Pay Money',
      value: '0',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Receive Money',
      value: '1',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Payment History',
      value: '2',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'My Paybag',
      value: '3',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Add Money to Paybag',
      value: '4',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Transfer Money to Bank',
      value: '5',
      checked: false
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data);
        let index = parseInt(data);

        let navTransition = alert.dismiss();
        navTransition.then( () => {
          switch(index) {
          case 0:
            this.navCtrl.push('Pay');
            this.toPay();
            break; 
          case 1:
            this.toReceive();
            break;
          case 2:
            this.toTransactions();
            break;
          case 3:
            this.toPaybag();
            break;
          case 4:
            this.toLoadPaybag();
            break;
          case 5:
            this.toPayout();
            break;
          default:
            break;
          };
        })
      }
      
    });
    alert.present();
  }

}
