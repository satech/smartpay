import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { SmartpayTranslateLoader } from '../../providers/smartpay-translate-loader';
import { Http } from '@angular/http';
import { HomePage } from './home';
import { Options } from '../options/options';

@NgModule({
  declarations: [
    HomePage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (SmartpayTranslateLoader),
        deps: [Http]
      }
    })    
  ],
  exports: [
    HomePage
  ]
})
export class HomePageModule {}
