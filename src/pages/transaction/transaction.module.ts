import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Transaction } from './transaction';

@NgModule({
  declarations: [
    Transaction,
  ],
  imports: [
    IonicPageModule.forChild(Transaction),
  ],
  exports: [
    Transaction
  ]
})
export class TransactionModule {}
