import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Payment } from '../../providers/payment';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class Transactions {

  uid: any;
  transactionList: any;
  startKey: any;
  showList: any;
  showLoading: any;
  showEmpty: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public auth: Auth, public payment: Payment) {
    this.transactionList = [];
    this.showLoading = true;
    this.showList = false;
    this.showEmpty = false;
     
	this.auth.getUser().then(u => {
	  let user = JSON.parse(u);      
    if(user.uid) {
		let userid = user.uid;
    this.startKey = '';
		this.uid = userid;

    this.payment.getSavedTransactions().then( (trans) => {
        
        if(trans) {
          this.transactionList = [];
          trans.forEach( (transaction) => {
            this.transactionList.push(transaction);
          });
          this.showList = true;
          this.showLoading = false;
          this.showEmpty = false;
        } else {
          this.showEmpty = true;
          this.showLoading = false;
          this.showList = false;
        }

        return this.payment.getStartTransactions(this.uid, 10);
      })
      .then( (payments) => {
        
      if(payments && payments.length > 1) {
        this.transactionList = [];
        payments.forEach( (transaction) => {
            this.transactionList.push(transaction);
        });
        this.showList = true;
        this.showLoading = false;
        this.showEmpty = false;
        this.payment.saveTransactions(payments);
      } else {
        if(this.transactionList.length < 1) {
          this.showEmpty = true;
          this.showLoading = false;
          this.showList = false;
        }
      }
    }); 
      }
  	});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Transactions');
  }
  
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    
    if(this.transactionList.length > 0)
	    this.startKey = this.transactionList[this.transactionList.length - 1].reverseTimestamp;

		let _self = this;
    setTimeout(() => {
	
	  _self.payment.getTransactions(_self.uid, _self.startKey, 10).then( (payments) => {
      console.log(payments);
      if(payments && payments.length > 1) {
        payments.forEach( (payment) => {
          _self.transactionList.push(payment);
        });

        if(_self.transactionList.length > 0) {
          var items = _self.transactionList.slice(0, 10);
          _self.payment.saveTransactions(items);                    
        }

        _self.showList = true;
        _self.showLoading = false;
        _self.showEmpty = false;
      }
    });
	 
    
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  getFirstLetter(fullName) {

  }

  refreshPage() {
    this.navCtrl.push(this.navCtrl.getActive().component);
  }

}
