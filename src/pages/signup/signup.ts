import { Component } from '@angular/core';
import { App, IonicPage, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Auth } from '../../providers/auth';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  email: any;
  password: any;
  loader: any;
  navCtrl: any;

  constructor(public app: App, public auth: Auth, public storage: Storage, public loadr: LoadingController) {
    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Signing up..."
    });

    this.navCtrl = this.app.getActiveNav();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  signup() {
    
    let user = {
      email: this.email,
      password: this.password
    };
    console.log(!user.email);
    if(!user.email || !user.password) {
      alert("Email and password required");
      return;
    }
    this.loader.present();
    this.auth.signup(user)
      .then((response) => {
        let currentuser = {
          email: user.email,
          uid: response.uid
        }
        this.storage.set('currentuser', JSON.stringify(currentuser));
        this.navCtrl.push('SetupPage', {user: currentuser});
        this.loader.dismiss();        
      })
      .catch((error) => {
        this.loader.dismiss();
        // Handle Errors here.
        //var errorCode = error.code;
        var errorMessage = error.message;
        
        alert(errorMessage);
      });
  }

  toLoginPage() {
    this.navCtrl.push('LoginPage');
  }

}
