import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Payout } from './payout';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';

@NgModule({
  declarations: [
    Payout,
  ],
  imports: [
    IonicPageModule.forChild(Payout),
  ],
  exports: [
    Payout
  ],
  providers: [
    Profile,
    FirebaseService
  ]
})
export class PayoutModule {}
