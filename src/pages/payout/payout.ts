import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';

/**
 * Generated class for the Payout page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-payout',
  templateUrl: 'payout.html',
})
export class Payout {
  profile: any;
  uid: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fservice: FirebaseService) {
    

    this.fservice.getCurrentUser()
      
      .subscribe( (user) => {
        this.uid = user.uid;
        this.fservice.getSavedProfile()
          
          .subscribe( (sprofile) => {
            if(sprofile && sprofile.phoneNumber) {
              this.profile = sprofile;
            }

            this.fservice.getUserProfile(this.uid)
              
              .subscribe( (oprofile) => {
                if(oprofile && oprofile.phoneNumber) {
                  console.log(oprofile);
                  this.profile = oprofile;
                } else {
                  
                }
              })
          })
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Payout');
  }

}
