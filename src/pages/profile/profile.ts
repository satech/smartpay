import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Auth } from '../../providers/auth';
import { Profile } from '../../providers/profile';
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  uid: any = null;
  profile: FirebaseObjectObservable<any>;
  showProfile: any;
  showLoading: any;
  showOffline: any;
  navCtrl: any;

  constructor(public app: App, public navParams: NavParams, public db: AngularFireDatabase, 
    public auth: Auth, public profle: Profile, public fservice: FirebaseService) {
    let userid = navParams.get('uid');
    this.navCtrl = this.app.getActiveNav();
    this.profile = FirebaseObjectObservable.create();
  	
    this.uid = userid;

    this.showLoading = true;
    this.showProfile = false;
    this.showOffline = false;

    let _self = this;
    let timeOut = setTimeout(function(e) {      
      _self.showLoading = false;
      if(this.profile && this.profile.phoneNumber) {
        _self.showProfile = true;
        _self.showOffline = false;
      } else {
        _self.showProfile = false;
        _self.showOffline = true;
      }
    }, 5000);    

    this.fservice.getCurrentUser()
      .subscribe( (user) => {
        this.uid = user.uid;
        this.fservice.getSavedProfile()
          .subscribe( (sprofile) => {
            if(sprofile && sprofile.phoneNumber) {
              this.profile = sprofile;
              this.showProfile = true;
              this.showLoading = false;
              this.showOffline = false;
              clearTimeout(timeOut);
            }

            this.fservice.getUserProfile(this.uid)
              .subscribe( (sprofile) => {
                if(sprofile && sprofile.phoneNumber) {
                  this.profile = sprofile;
                  this.showProfile = true;
                  this.showLoading = false;
                  this.showOffline = false;
                  clearTimeout(timeOut);
                } else {
                  let confm = confirm("You must setup your paybag before making payment. Do you want to set it up now?");
                  if(confm) {
                    this.navCtrl.setRoot('SetupPage');
                  } else {
                    this.navCtrl.pop();
                  }
                }
              })
          })
      });
    
  	
  }

  refreshPage() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }  

  toEditProfile() {
    this.navCtrl.push('EditProfile', {uid: this.uid});
  }

  toEditAccount() {
    this.navCtrl.push('EditAccount', {uid: this.uid});
  }

}
