import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Auth } from '../../providers/auth';
import { Profile } from '../../providers/profile';
import { Payment } from '../../providers/payment';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: any;
  password: any;
  loader: any;
  navCtrl: any;

  constructor(public app: App, public loadr: LoadingController, public auth: Auth, 
  public storage: Storage, public profile: Profile, public payment: Payment) {
    this.navCtrl = this.app.getActiveNav();
    this.loader = this.loadr.create({
      spinner: 'crescent',
      content: "Logging in..."
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  login() {
    let user = {
      email: this.email,
      password: this.password
    };

    if(!user.email || !user.password) {
      alert("Email and password required");
      return;
    }
    let _self = this;
    setTimeout(function() {
      _self.loader.present();
      _self.auth.login(user)
        .then((response) => {
          if(response.uid) {
            _self.profile.getUserProfile(response.uid)
              .then( (data) => {                
                if(data) {
                  _self.storage.set('profile', JSON.stringify(data));                   
                  _self.payment.loadTransactions(response.uid);
                  _self.profile.loadProfile(response.uid);
                  _self.profile.loadAccount(response.uid);
                  _self.profile.loadPaybag(response.uid);                 
                }
                _self.loader.dismiss();
                _self.storage.set('currentuser', JSON.stringify(response));
                _self.navCtrl.setRoot('HomePage', {user:JSON.stringify(response)});
              });
          }
                  
        })
        .catch((error) => {
          // Handle Errors here.
          //var errorCode = error.code;
          var errorMessage = error.message;
          _self.loader.dismiss();
          alert(errorMessage);        
          console.log(error);
        });
    }, 1000);
  }

  toSignupPage() {
    this.navCtrl.push('SignupPage');
  }

}
