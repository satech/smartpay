import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { Http, Headers } from '@angular/http';

@Injectable()
export class FirebaseService {

  APIKEY: any;
  constructor(public db: AngularFireDatabase, public storage: Storage, public http: Http) {
    this.APIKEY = "ts_EKIFKPM5IJIOI6DPUCKU";
  }

  getUserProfile(uid) {
    return this.db.object('users/' + uid + '/profile');
  }

  getUserAccount(uid) {
    return this.db.object('users/' + uid + '/accounts').map( account => account.$value );
  }

  getUserPaybag(uid) {
    return this.db.object('users/' + uid + '/paybag');
  }


  getSystemDefault() {
    return this.db.object('systemDefault/bank');
  }

  getCurrentUser() {
    return Observable.fromPromise(
      this.storage.get('currentuser')
        .then( (user) => {
          return JSON.parse(user);
        })
    );
  }

  getSavedAccount() {
    return Observable.fromPromise(
      this.storage.get('account')
        .then( (account) => {
          return JSON.parse(account);
        })
    );
  }

  getSavedProfile() {
    return Observable.fromPromise(
      this.storage.get('profile')
        .then( (profile) => {
          return JSON.parse(profile);
        })
    );
  }

  getSavedPaybag() {
    return Observable.fromPromise(
      this.storage.get('paybag')
        .then( (paybag) => {
          return JSON.parse(paybag);
        })
    );
  }

  getItemOfflineOrOnline(url, key): Observable<any> {
    return Observable.fromPromise(this.storage.get(key)
      .then((param) => {
        if (param) {
          return JSON.parse(param);
        } else {
          return this.db.object(url)
            .map( res => res.json());      
        }
      })
    );
  }

  getItemOfflineAndOnline(url, key): Observable<any> {
    return Observable.fromPromise(this.storage.get(key)
      .then((param) => {
        if (param) {
          return JSON.parse(param);
        } 
        return this.db.object(url)
          .map( res => res.json());
      })
    );
  }

  createTimeout(timeout) {
      return new Promise((resolve, reject) => {
          setTimeout(() => resolve(null),timeout)
      })
  }

  setTime(timeout): Observable<any> {
    return Observable.fromPromise(this.createTimeout(timeout));
  }

  authMoneywave(): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    
    let url = 'https://moneywave.herokuapp.com/v1/merchant/verify';
    let body = { "apiKey": "ts_EKIFKPM5IJIOI6DPUCKU", "secret": "ts_HISBMEB3ERA97C6W4M01DX9ERZ2ILV" }

    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }

  cardToAccountTransfer(token, data): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    let url = 'https://moneywave.herokuapp.com/v1/transfer';
    var  body = {
      "firstname": "John",
      "lastname": "Doe",
      "email":"google@gmail.com",
      "phonenumber":"+2348200990020",
      "recipient_bank": "044",
      "recipient_account_number": "0690000008",
      "card_no": "5289899898983388",
      "cvv": "788",
      "pin":"8989", //optional required when using VERVE card
      "expiry_year":"2022",
      "expiry_month": "09",
        "charge_auth":"PIN", //optional required where card is a local Mastercard
      "apiKey" :"ts_EKIFKPM5IJIOI6DPUCKU",
      "amount" :5000,
      "narration":"Gucchi shirt payment", //Optional
      "fee":45,
      "medium": "web",
      "redirectUrl": "https://google.com"
    };
    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }

  cardToAccountValidation(token, data): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    let url = 'https://moneywave.herokuapp.com/v1/transfer/charge/auth/card';
    let body = data;

    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }

  accountToAccountTransfer(token, data): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    let url = 'https://moneywave.herokuapp.com/v1/transfer';
    let body = data;
    body.apiKey = this.APIKEY;

    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }

  accountToAccountValidation(token, data): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    let url = 'https://moneywave.herokuapp.com/v1/transfer/charge/auth/account';
    let body = data;
    console.log(data)

    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }
  
  validateAccount(token, data): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    let url = 'https://moneywave.herokuapp.com/v1/resolve/account';
	  let body = data;
    return this.http.post(url, body, {headers: headers})
      .map( res => res.json());
  }
  
  getBankByID(id) {
	let data = 
	   {
     "214": "FIRST CITY MONUMENT BANK PLC",
		 "215": "UNITY BANK PLC",
		 "221": "STANBIC IBTC BANK PLC",
		 "232": "STERLING BANK PLC",
		 "304": "Stanbic Mobile",
		 "305": "PAYCOM",
		 "307": "Ecobank Mobile",
		 "309": "FBN MOBILE",
		 "311": "Parkway",
		 "315": "GTBank Mobile Money",
		 "322": "ZENITH Mobile",
		 "323": "ACCESS MOBILE",
		 "401": "Aso Savings and Loans",
		 "044": "ACCESS BANK NIGERIA",
		 "014": "AFRIBANK NIGERIA PLC",
		 "063": "DIAMOND BANK PLC",
		 "050": "ECOBANK NIGERIA PLC",
		 "084": "ENTERPRISE BANK LIMITED",
		 "070": "FIDELITY BANK PLC",
		 "011": "FIRST BANK PLC",
		 "058": "GTBANK PLC",
		 "030": "HERITAGE BANK",
		 "082": "KEYSTONE BANK PLC",
		 "076": "SKYE BANK PLC",
		 "068": "STANDARD CHARTERED BANK NIGERIA LIMITED",
		 "032": "UNION BANK OF NIGERIA PLC",
		 "033": "UNITED BANK FOR AFRICA PLC",
		 "035": "WEMA BANK PLC",
		 "057": "ZENITH BANK PLC" 
	}
	return data[id];
  }
  
  getBanks() {
	let banks = [
		{ id: "214", name: "FIRST CITY MONUMENT BANK PLC" },
		{ id: "215", name: "UNITY BANK PLC" },
		{ id: "221", name: "STANBIC IBTC BANK PLC" },
		{ id: "232", name: "STERLING BANK PLC" },
		{ id: "304", name: "Stanbic Mobile" },
		{ id: "305", name: "PAYCOM" },
		{ id: "307", name: "Ecobank Mobile" },
		{ id: "309", name: "FBN MOBILE" },
		{ id: "311", name: "Parkway" },
		{ id: "315", name: "GTBank Mobile Money" },
		{ id: "322", name: "ZENITH Mobile" },
		{ id: "323", name: "ACCESS MOBILE" },
		{ id: "401", name: "Aso Savings and Loans" },
		{ id: "044", name: "ACCESS BANK NIGERIA" },
		{ id: "014", name: "AFRIBANK NIGERIA PLC" },
		{ id: "063", name: "DIAMOND BANK PLC" },
		{ id: "050", name: "ECOBANK NIGERIA PLC" },
		{ id: "084", name: "ENTERPRISE BANK LIMITED" },
		{ id: "070", name: "FIDELITY BANK PLC" },
		{ id: "011", name: "FIRST BANK PLC" },
		{ id: "058", name: "GTBANK PLC" },
		{ id: "030", name: "HERITAGE BANK" },
		{ id: "082", name: "KEYSTONE BANK PLC" },
		{ id: "076", name: "SKYE BANK PLC" },
		{ id: "068", name: "STANDARD CHARTERED BANK NIGERIA LIMITED" },
		{ id: "032", name: "UNION BANK OF NIGERIA PLC" },
		{ id: "033", name: "UNITED BANK FOR AFRICA PLC" },
		{ id: "035", name: "WEMA BANK PLC" },
		{ id: "057", name: "ZENITH BANK PLC" }
	];		
	
	return banks;
  }

  presentLoading(_self) {
    let loading = _self.loader.create({
        spinner: 'crescent',
        content: "processing ..."
      });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
    return loading;
  }

  dismissLoading(loading) {
    loading.dismiss();
  }

}