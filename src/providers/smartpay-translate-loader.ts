import { Injectable } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SmartpayTranslateLoader provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SmartpayTranslateLoader {

  constructor(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }

}
