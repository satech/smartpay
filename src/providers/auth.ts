import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class Auth {

  user: Observable<firebase.User>;
  constructor(public storage: Storage, public afAuth: AngularFireAuth, public db: AngularFireDatabase) {    
    this.user = afAuth.authState;
  }

  loginGplus() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  
  getSeen() {
    return this.storage.get('startSeen').then((val) => {
      return val;
    });
  }

  loggedIn() {
    return this.afAuth.authState;
  }

  isLoggedIn() {
    return this.storage.get('currentuser');
  }

  getUser() {
    return this.storage.get('currentuser');    
  }

  setSeen() {
    this.storage.set('startSeen', 1);
  }

  clearSeen() {
    this.storage.remove('startSeen');
  }

  login(user) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);      
  }

  signup(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);      
  }

  logout() {
    return this.afAuth.auth.signOut()
      .then(() => {
        this.storage.remove('currentuser');
        this.storage.remove('profile');
        this.storage.remove('account');
        this.storage.remove('paybag');
        this.storage.remove('transactions');
      });
  }

}
