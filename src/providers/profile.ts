import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class Profile {
  user: Observable<firebase.User>;
  profile: FirebaseObjectObservable<any>;
  account: FirebaseListObservable<any>;
  connected: any = false;
  constructor(public storage: Storage, public afAuth: AngularFireAuth, public db: AngularFireDatabase) {
    
    this.user = afAuth.authState;
    
  }

  getProfile(uid) {
    return this.db.object('/users/' + uid + '/profile');	
  }

  getPaybag(uid) {
    return firebase.database().ref('/users/' + uid).child('paybag')
      .once('value').then( (u) => {
        return u.val();
       });	
  }

  getUserData(uid) {
    return firebase.database().ref('users').child(uid)
      .once('value').then( (u) => {
        return u.val();
       });	
  }

  getIDBySmartPayID(smartPayID) {
    return firebase.database().ref('unique').child(smartPayID)
      .once('value').then( (u) => {
        return u.val();
       });
  }

  getPaybagBySmartPayID(smartPayID) {
    return this.getIDBySmartPayID(smartPayID)
      .then( (sid) => {
        if(sid) {
          return firebase.database().ref('users').child(sid).child('paybag').once('value')
            .then( (p) => { 
              let paybag = p.val();
              if(paybag) {
                paybag.uid = sid;
                return paybag;
              } else {
                return false;
              }
             });
        } else {
          return false;
        }
      });
  }

  toTrue() {
	  return true;
  }

  toFalse() {
	  return false;
  }

  getUserBySmartPayID(smartPayID) {
    return this.getIDBySmartPayID(smartPayID)
      .then( (sid) => {
        if(sid) {
          return firebase.database().ref('users').child(sid).once('value')
            .then( (p) => { 
              let user = p.val();
              if(user) {
                user.uid = sid;
                return user;
              } else {
                return false;
              }
             });
        } else {
          return false;
        }
      });
  }

  createProfile(uid, profile_data, paybag_data) {
    this.profile = this.db.object('/users/' + uid);
    return this.profile.set({ profile: profile_data, paybag: paybag_data });
  }
  
  addAccount(uid, account) {
	this.account = this.db.list('/users/' + uid + '/accounts');
	return this.account.push(account);
  }
  
  addUniqueID(uid, smartPayID) {
    this.profile = this.db.object('/unique/' + smartPayID);
    return this.profile.set( uid );
  }
  
  addSampleTransaction(uid) {
    this.profile = this.db.object('/transactions/' + uid);
	let transactiondata = [
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 23.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
},
{
 amount: 123.9,
 description: "description",
 fromSmartPayID: "09080",
 reverseTimestamp: -1,
 toSmartPayID: "08708",
 timestamp: 1,
 type: 1,
 status: 1
}
];

    this.profile.set( transactiondata );
  }
  
  getUniqueID(smartPayID) {
    return firebase.database().ref('/unique/' + smartPayID).once('value')
      .then( (snap) => { return snap.val() } );
  }

  update(uid, profile_data) {
    this.profile = this.db.object('/users/' + uid);
    return this.profile.update({ profile: profile_data });
  }

  delete(uid) {
    this.profile = this.db.object('/users/uid');
    this.profile.remove();
  }
  
  isOnline() {
    let connectedRef = firebase.database().ref(".info/connected");
    connectedRef.on("value", function(snap) {
      if (snap.val() === true) {
        this.connected = true;
      } else {
        this.connected = false;
      }
    });
  }

  isLoggedIn() {
    return this.storage.get('currentuser');
  }

  setSeen() {
    this.storage.set('startSeen', 1);
  }

  clearSeen() {
    this.storage.remove('startSeen');
  }

  login(user) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);      
  }

  signup(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);      
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  getUserProfile(uid) {
    return firebase.database().ref('/users/' + uid + '/profile').once('value')
    .then( (snap) => { return snap.val() } );
  }

  getAccount(uid) {
    return firebase.database().ref('/users/' + uid + '/accounts').once('value')
      .then( (snap) => {         
        let a = [];
        snap.forEach( (acct) => {
          a.push(acct.val());
        })
        return a[0];
    } );
  } 

  getSavedUserProfile(uid = '') {
    return this.storage.get('profile')
      .then( (user) => {
        return JSON.parse(user);
      });
  }

  getSavedPaybag() {
    return this.storage.get('paybag')
      .then( (bag) => {
        return JSON.parse(bag);
      });
  }

  saveProfile(profile) {
    if(profile)
      this.storage.set('profile', JSON.stringify(profile));
  }

  saveAccount(acct) {
    if(acct)
      this.storage.set('account', JSON.stringify(acct));
  }

  savePaybag(paybag) {
    if(paybag)
      this.storage.set('paybag', JSON.stringify(paybag));
  }

  loadPaybag(uid) {
    this.getPaybag(uid).then( (bag) => {
      if(bag) {
          this.savePaybag(bag);
      }
    });
  }

  loadAccount(uid) {
    this.getAccount(uid).then( (acct) => {
      console.log(acct);
      if(acct) {
          this.saveAccount(acct);
      }
    });
  }

  loadProfile(uid) {
    this.getUserProfile(uid).then( (profile) => {
      if(profile) {
          this.savePaybag(profile);
      }
    });
  }

}
