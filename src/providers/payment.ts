import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Profile } from './profile';

/*
  Generated class for the Payment provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Payment {

  commision: any = 0.01;
  payment: FirebaseObjectObservable<any>;
  toPayment: any;
  fromPayment: any;
  toPaybag: any;
  fromPaybag: any;
  constructor(public db: AngularFireDatabase, public profile: Profile, public storage: Storage) {
    console.log('Hello Payment Provider');
    this.toPayment = {};
    this.fromPayment = {};
  }

  randomString (length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  getRandonValue(arr) {
    let d = arr;
    return d[Math.floor(Math.random() * d.length)];
  }

  isValidData(payment) {
    if(!payment.fromSmartPayID || !payment.toSmartPayID || !payment.amount || !payment.description) {
      return false;
    } else {
      return true;
    }
  }

  isValidPayment(payment) {
    if(!payment.fromUID || !payment.toUID || !payment.fromSmartPayID || !payment.toSmartPayID || !payment.amount || !payment.description || !payment.transactionID) {
      return false;
    } else {
      return true;
    }
  }

  addPayment(uid, payment) {
    var timestamp = firebase.database.ServerValue.TIMESTAMP;
    payment.timestamp = timestamp;
        
    let paymentResponse = this.profile.getUserData(uid)
      .then( (fromData) => {
        let fromPaybagData = fromData.paybag;
        let fromProfileData = fromData.profile;
        if(fromData && fromPaybagData) {
          const fromBalance = (parseFloat(fromPaybagData.balance) * 100) / 100;
          const amount = parseFloat(payment.amount);
          const tcommision = ((amount * this.commision) * 100) / 100;
          const fromNewBalance = fromBalance - amount - tcommision;
          this.fromPaybag = {};
          this.fromPaybag.balance = Math.round(fromNewBalance * 10000) / 10000;
          this.fromPaybag.prevBalance = fromBalance;
          this.fromPaybag.transaction = ((amount + tcommision) * 100) / 100;
          this.fromPaybag.timestamp = timestamp;
          
          this.fromPayment.fromSmartPayID = payment.fromSmartPayID;
          this.fromPayment.toSmartPayID = payment.toSmartPayID;
          this.fromPayment.amount = payment.amount;
          this.fromPayment.timestamp = timestamp;
          if(payment.transactionID)
            this.fromPayment.transactionID = payment.transactionID;
          this.fromPayment.name = fromProfileData.firstname + ' ' + fromProfileData.surname;
          this.fromPayment.type = 1;
          this.fromPayment.description = (payment.description)? payment.description: 'n/a';
          this.fromPayment.commision = tcommision;
          this.fromPayment.status = 1;
          
          console.log(this.fromPayment);
          if(fromNewBalance > 0) {
            return this.profile.getUserBySmartPayID(payment.toSmartPayID);
          } else {
            let err = "Insufficient funds. Please fund your Paybag and retry.";            
            return Promise.reject(err);
          }
        } else {
          let err = "You've not setup your Paybag. Please do so to start paying and receiving money easily.";
          return Promise.reject(err);
        } 
      })
      .then( (toData) => {
        let toPaybagData = toData.paybag;
        let toProfileData = toData.profile;
        if(toData && toPaybagData) {
          //alert(JSON.stringify(toData));
          const toBalance = (parseFloat(toPaybagData.balance) * 100) / 100;
          const amount = parseFloat(payment.amount);
          const toNewBalance = ((toBalance + amount) * 100) / 100;
          let  userid = toData.uid;
          this.toPaybag = {};
          this.toPaybag.balance = Math.round(toNewBalance * 10000) / 10000;
          this.toPaybag.prevBalance = toBalance;
          this.toPaybag.transaction = amount;
          this.toPaybag.timestamp = timestamp;          
          
          this.toPayment.fromSmartPayID = payment.fromSmartPayID;
          this.toPayment.toSmartPayID = payment.toSmartPayID;
          this.toPayment.amount = payment.amount;
          this.toPayment.timestamp = timestamp;
          if(payment.transactionID)
            this.toPayment.transactionID = payment.transactionID;
          this.toPayment.name = toProfileData.firstname + ' ' + toProfileData.surname;
          this.toPayment.type = 2;
          this.toPayment.description = (payment.description)? payment.description: 'n/a';
          this.toPayment.status = 1;          
                    
          var key = firebase.database().ref('/transactions/' + uid).push().key;
          let fromData = {};
          fromData[key] = this.fromPayment;
          return firebase.database().ref('/transactions/' + uid).child(key)
            .set( this.fromPayment ).then( (d) => {
              return firebase.database().ref('/transactions/' + uid).child(key).once('value')
                .then( (transac) => {
                  let  transaction = transac.val();
                  if(transaction) {
                    let update = transaction.timestamp;
                    this.fromPayment.reverseTimestamp = 0 - update;
                    this.fromPayment.timestamp = update;
                    return firebase.database().ref('/transactions/' + uid).child(key).update( this.fromPayment )
                      .then( () => {
                        return firebase.database().ref('/users/' + uid).child('paybag').update( this.fromPaybag )
                      })
                      .then( () => {
                        var key = firebase.database().ref('/transactions/' + this.toPaybag.uid).push().key;
                        let toData = {};
                        toData[key] = this.toPayment;
                        return firebase.database().ref('/transactions/' + userid).child(key)
                          .set( this.toPayment ).then( (d) => {
                            return firebase.database().ref('/transactions/' + userid).child(key).once('value')
                              .then( (transac) => {
                                let  transaction = transac.val();
                                console.log(transaction);
                                if(transaction) {
                                  let update = transaction.timestamp;
                                  this.toPayment.reverseTimestamp = 0 - update;
                                  return firebase.database().ref('/transactions/' + userid).child(key).update( this.toPayment )
                                    .then( () => {
                                      firebase.database().ref('/users/' + userid).child('paybag').update( this.toPaybag )
                                      .then( () => {
                                        return this.fromPayment;
                                      });                                      
                                    });
                                } else {
                                  let err = "Error occured while processing your request. Please retry.";
                                  return Promise.reject(err);
                                }
                              });
                          });
                      });
                  } else {
                    let err = "Error occured while processing your request. Please retry.";
                    return Promise.reject(err);
                  }
                });
            });
          
        } else {
          let err = "Recepient's Paybag  in not setup. Please tell them to activate their Paybag to start receiving money.";
          return Promise.reject(err);
        }
      })
      .catch( (err) => {
        console.log(err);
        return Promise.reject(err);        
      });

      return paymentResponse.then( (response) => {
        return Promise.resolve(this.fromPayment);
      }).catch( (err) => {
        return Promise.reject(err);
      });
    
  }

  pushPayment(uid, payment) {
    let timeStamp = firebase.database.ServerValue.TIMESTAMP;
    if(this.isValidPayment(payment)) {

    }

  }

  getTransactions(uid, startKey, total) {
    console.log(startKey + " | " + uid);
    
    return firebase.database().ref('transactions').child(uid).orderByChild('reverseTimestamp')
    .startAt(startKey)
    .limitToFirst(total + 1).once('value').then( (snapshots) => {
      console.log(snapshots);
      let arr = [];
      snapshots.forEach( (snapshot) => {
      let val = snapshot.val();
      if(val) {
        val.key = snapshot.key;
        arr.push(val);
      }
      });
      arr.shift();
      return arr;
    });
  }

  getStartTransactions(uid, total) {
    console.log(uid);
    return firebase.database().ref('transactions').child(uid).orderByChild('reverseTimestamp')
    .limitToFirst(total).once('value').then( (snapshots) => {
      let arr = [];
      snapshots.forEach( (snapshot) => {
      let val = snapshot.val();
      if(val) {
        val.key = snapshot.key;
        arr.push(val);
      }
      });
      return arr;
    });
  }

  getSavedTransactions() {
    return this.storage.get('transactions')
      .then( (transactions) => {
        console.log(transactions);
        return JSON.parse(transactions);
      });
  }

  saveTransactions(transactions) {
    this.storage.set('transactions', JSON.stringify(transactions));
  }

  loadTransactions(uid) {
    this.getStartTransactions(uid, 20).then( (payments) => {
      if(payments && payments.length > 1) {
        let trans = [];
        payments.forEach( (payment) => {
          trans.push(payment);
        });

        if(trans.length > 0) {
          this.saveTransactions(trans);
        }
      }
    });
  }

}
