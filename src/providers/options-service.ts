import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the OptionsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class OptionsService {

  static get PREF_INITIALIZED() { return 'settings';}
  static get PREF_LANGUAGE() { return 'pref_language';}
  static get PREF_NOTIFY_MESSAGES() { return 'pref_notification_messages';}
  static get PREF_NOTIFY_INVITES() { return 'pref_notification_invites';}

  _settings: any;
  constructor(public http: Http, public storage: Storage) {
    this._settings = {};
    this.initializePreferences();
  }

  initializePreferences(){
    console.log('initializePreferences');
    this.storage.get(OptionsService.PREF_INITIALIZED).then((result) => {
      console.log('result', result);
      if(result == null || result == false){
        let options = {
          pref_language: "en"
        }
        this.storage.set(OptionsService.PREF_INITIALIZED, JSON.stringify(options));

        //initialize in memory preferences
        this._settings[OptionsService.PREF_LANGUAGE] = 'en';
      }else{
        console.log('preferences obtained from storage');
        let prefs =
          [
            OptionsService.PREF_LANGUAGE
          ];

        let thisRef = this;
        this._settings = JSON.parse(result);
        /*
        this._getAllPreferences(prefs).then(function(results){
            //initialize in memory preferences
            for(let i = 0; i < prefs.length; i++){
              thisRef._settings[prefs[i]] = results[i];
            }
          }, function (err) {
            // If any of the preferences fail to read, err is the first error
            console.log(err);
          }); */
      }
    });
  }

  getPreference(key){
    return this._settings[key];
  }

  getPreferences(){
    return this.storage.get(OptionsService.PREF_INITIALIZED);
  }

  setPreference(key, value){
    this._settings[key] = value;//update pref in memory
    this.storage.set(OptionsService.PREF_INITIALIZED, JSON.stringify(this._settings));//update pref in db
  }

  _getAllPreferences(prefs){
    return Promise.all(prefs.map((key) => {
      return this.storage.get(key);
    }));
  }

  _getPreference(key) {
    return this.storage.get(key)
    .then( (val) => {
      let result = JSON.parse(val);
      return result.key;
    });    
  }


}
